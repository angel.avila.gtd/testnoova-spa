import I18N from './config/modules/i18n'
import ELEMENT from './config/modules/element'
import AXIOS from './config/modules/axios'
import AUTH from './config/modules/auth'

const path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}

// ------------------------------------------------------------
// SETUP ENVIRONMENT
// ------------------------------------------------------------
var TESTNOOVA_API = 'https://localhost:44310/api'

if (process.env.NODE_ENV === 'production') {
  TESTNOOVA_API = 'https://localhost:44310/api'
}
// ------------------------------------------------------------

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#eb4c34' },
  /*
  ** Global CSS
  */
  css: [
    'element-ui/lib/theme-chalk/index.css',
    '@assets/styles/index.scss'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    { src: '@plugins/element-ui.js' },
    { src: '@plugins/icons.js' },
    { src: '@plugins/vuelidate.js' }
  ],
  /*
  ** Server Middleware
  */
  serverMiddleware: [],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    [
      'nuxt-element-ui', ELEMENT
    ],
    [
      'nuxt-i18n', I18N
    ],
    [
      '@nuxtjs/axios', AXIOS
    ],
    [
      '@nuxtjs/auth', AUTH
    ]
  ],
  /*
  ** Proxy configuration
  */
  proxy: {
    '/testnoova': { target: TESTNOOVA_API, pathRewrite: { '^/testnoova/': '' } }
  },
  /*
  ** OAuth
  */
  router: {
    middleware: [/*'auth', 'scopes', */'lang']
  },
  /*
  ** Build configuration
  */
  vue: {
    config: {
      productionTip: false,
      devtools: true
    }
  },
  server: {
    port: 3000, // default: 3000
    host: '0.0.0.0' // default: localhost
  },
  build: {
    extend(config, { isDev, isClient }) {
      const svgRule = config.module.rules.find(rule => rule.test.test('.svg'))
      svgRule.exclude = [resolve('assets/icons/svg')]

      config.module.rules.push({
        test: /\.svg$/,
        include: [resolve('assets/icons/svg')],
        use: [{ loader: 'svg-sprite-loader', options: { symbolId: 'icon-[name]' } }]
      })

      // Sets webpack's mode to development if `isDev` is true.
      if (isDev) { config.mode = 'development' }
    }
  }
}
