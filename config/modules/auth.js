export default {
    plugins: [],
    rewriteRedirects: false,
    resetOnError: true,
    // fullPathRedirect: true,
    redirect: {
        login: '/login',
        logout: false,
        callback: '/callback',
        home: '/'
    },
    strategies: {
        auth0: {
            domain: 'devpedrofernando.auth0.com',
            client_id: '1bm4lA22FUkt9Mugp01TOiOVRtwUwr3H',
            audience: 'https://api.gtd.com.co'
        }
    },
    scopeKey: 'permissions',
    cookie: {
        prefix: 'auth.',
        options: {
            path: '/',
            maxAge: 86400,
            secure: false
        }
    }
}