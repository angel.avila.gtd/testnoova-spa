import enLocale from 'element-ui/lib/locale/lang/en'
import esLocale from 'element-ui/lib/locale/lang/es'

const I18N = {
    locales: ['en', 'es'],
    defaultLocale: 'es',
    vueI18n: {
        fallbackLocale: 'es',
        messages: {
            en: enLocale,
            es: esLocale
        }
    },
    vueI18nLoader: true
}

export default I18N