export default {
    proxy: false,
    proxyHeaders: false,
    debug: false,
    https: false,
    changeOrigin: true
}