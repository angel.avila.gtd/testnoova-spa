const items = [
    {
        path: 'external-link',
        meta: { title: 'Apps', icon: 'menu' },
        children: [
            {
                path: 'http://web.noova.com.co/e-invoice',
                meta: { title: 'e-Invoice', icon: 'fpi' }
            },
            {
                path: 'http://web.noova.com.co/e-commerce',
                meta: { title: 'e-Commerce', icon: 'inventory' }
            }
        ]
    },
    {
        path: '/',
        meta: { title: { es: 'Inicio', en: 'Home' }, icon: 'home' }
    },
    {
        path: '/creation',
        meta: { title: { es: 'Creacion', en: 'Creation' }, icon: 'plus', redirect: 'noRedirect' },
        children: [
            {
                path: 'customer',
                meta: { title: { es: 'Cliente', en: 'Customer' } }
            },
            {
                path: 'product',
                meta: { title: { es: 'Producto', en: 'Product' } }
            }
        ]
    },
    {
        path: '/management',
        meta: { title: { es: 'Administracion', en: 'Management' }, icon: 'gear', redirect: 'noRedirect' },
        children: [
            {
                path: 'customer',
                meta: { title: { es: 'Cliente', en: 'Customer' } }
            },
            {
                path: 'product',
                meta: { title: { es: 'Producto', en: 'Product' } }
            }
        ]
    }
]

export default items