import NavItems from '@/config/app/sidebarItems'

var jwt_decode = require('jwt-decode')

const SET_LANG = 'SET_LANG'
const SET_NAV_ITEMS = 'SET_NAV_ITEMS'

export const state = () => ({
    lang: 'en',
    navItems: JSON.parse(JSON.stringify(NavItems))
})

export const getters = {
    lang: state => state.lang,
    navItems: state => state.navItems
}

export const mutations = {
    [SET_LANG](state, lang) {
        state.lang = lang
    },
    [SET_NAV_ITEMS](state, items) {
        state.navItems = items
    }
}

export const actions = {
    nuxtServerInit({ commit }, context) {
        if (context.$auth.loggedIn) {
            var { permissions } = jwt_decode(context.$auth.getToken('auth0'))
            context.$auth.setUser(Object.assign(context.$auth.user, { permissions }))
        }
    },
    setLang({ commit }, lang) {
        commit(SET_LANG, lang)
    }
}