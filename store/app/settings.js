import defaultSettings from '@/config/app/settings'

const { showSettings, fixedHeader, sidebarLogo } = defaultSettings

export const state = () => ({
    showSettings: showSettings,
    fixedHeader: fixedHeader,
    sidebarLogo: sidebarLogo
})

export const getters = {
    showSettings: state => state.showSettings,
    fixedHeader: state => state.fixedHeader,
    sidebarLogo: state => state.sidebarLogo
}

export const mutations = {
    CHANGE_SETTING: (state, { key, value }) => {
        if (state.hasOwnProperty(key)) {
            state[key] = value
        }
    }
}

export const actions = {
    changeSetting({ commit }, data) {
        commit('CHANGE_SETTING', data)
    }
}