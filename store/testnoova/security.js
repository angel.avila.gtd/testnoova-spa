export const state = () => ({
    roles: {
        ROLE_ADMIN: "ROLE_ADMIN",
        ROLE_OPERATOR: "ROLE_OPERATOR"
    }
})

export const getters = {
    roles: state => state.roles
}

export const mutations = {
}

export const actions = {
}