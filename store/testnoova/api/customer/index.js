const ENDPOINT = 'https://localhost:44310/api/'
const ENDPOINT_CUSTOMERS = 'customer'
const ENDPOINT_ADDRESSES = 'address'
const ENDPOINT_CONTACTS = 'contact'
const ENDPOINT_PHONES = 'phone'

const CUSTOMER_SAVE = 'CUSTOMER_SAVE'
const FETCH_CUSTOMERS = 'FETCH_CUSTOMERS'
const CUSTOMER_UPDATE = 'CUSTOMER_UPDATE'
const CUSTOMER_DELETE = 'CUSTOMER_DELETE'
const FETCH_ADDRESSES = 'FETCH_ADDRESSES'
const FETCH_CONTACTS = 'FETCH_CONTACTS'
const FETCH_PHONES = 'FETCH_PHONES'
const ADDRESS_SAVE = 'ADDRESS_SAVE'
const ADDRESS_DELETE = 'ADDRESS_DELETE'
const CONTACT_SAVE = 'CONTACT_SAVE'
const CONTACT_DELETE = 'CONTACT_DELETE'
const PHONE_SAVE = 'PHONE_SAVE'
const PHONE_DELETE = 'PHONE_DELETE'


export const state = () => ({
    customers: [],
    addresses: [],
    contacts: [],
    phones: []
})

export const getters = {
    customers: state => state.customers,
    addresses: state => state.addresses,
    contacts: state => state.contacts,
    phones: state => state.phones
}

export const mutations = {
    [FETCH_CUSTOMERS]: (state, customers) => {
        state.customers = customers
    },
    [CUSTOMER_SAVE](state, customer) {
        state.customers.unshift(customer);
    },
    [CUSTOMER_UPDATE](state, customer) {
        const index = state.customers.findIndex(c => c.id === customer.id);
        state.customers[index] = customer;
    },
    [CUSTOMER_DELETE](state, id) {
        const index = state.customers.findIndex(c => c.id === id);
        state.customers.splice(index, 1);
    },
    [FETCH_ADDRESSES]: (state, addresses) => {
        state.addresses = addresses
    },
    [FETCH_CONTACTS]: (state, contacts) => {
        state.contacts = contacts
    },
    [FETCH_PHONES]: (state, phones) => {
        state.phones = phones
    },
    [ADDRESS_SAVE](state, address) {
        state.addresses.unshift(address);
    },
    [ADDRESS_DELETE](state, id) {
        const index = state.addresses.findIndex(a => a.id === id);
        state.addresses.splice(index, 1);
    },
    [CONTACT_SAVE](state, contact) {
        state.contacts.unshift(contact);
    },
    [CONTACT_DELETE](state, id) {
        const index = state.contacts.findIndex(c => c.id === id);
        state.contacts.splice(index, 1);
    },
    [PHONE_SAVE](state, phone) {
        state.phones.unshift(phone);
    },
    [PHONE_DELETE](state, id) {
        const index = state.phones.findIndex(p => p.id === id);
        state.phones.splice(index, 1);
    },
}

export const actions = {
    async fetchCustomers({ commit }, filter) {
        let url = `${ENDPOINT}${ENDPOINT_CUSTOMERS}`
        if (!!filter) {
            url+= `/filter/${filter}`
        }
        const result = await this.$axios.$get(url)
        commit(FETCH_CUSTOMERS, result)
    },
    async saveCustomer({ commit }, customer) {
        await this.$axios
            .$post(`${ENDPOINT}${ENDPOINT_CUSTOMERS}`, customer)
            .then(response => {
            commit(CUSTOMER_SAVE, response);
        })
    },
    async deleteCustomer({ commit }, id) {
        let url = `${ENDPOINT}${ENDPOINT_CUSTOMERS}/${id}`
        await this.$axios
            .$delete(url)
            .then(response => {
            commit(CUSTOMER_DELETE, response);
        })
    },
    async updateCustomer({ commit }, customer) {
        let url = `${ENDPOINT}${ENDPOINT_CUSTOMERS}/${customer.id}`
        await this.$axios
            .$put(url, customer)
            .then(response => {
            commit(CUSTOMER_UPDATE, response);
        })
    },
    async fetchAddresses({ commit }, id) {
        let url = `${ENDPOINT}${ENDPOINT_CUSTOMERS}/${id}/addresses`
        const result = await this.$axios.$get(url)
        commit(FETCH_ADDRESSES, result)
    },
    async fetchContacts({ commit }, id) {
        let url = `${ENDPOINT}${ENDPOINT_CUSTOMERS}/${id}/contacts`
        const result = await this.$axios.$get(url)
        commit(FETCH_CONTACTS, result)
    },
    async fetchPhones({ commit }, id) {
        let url = `${ENDPOINT}${ENDPOINT_CUSTOMERS}/${id}/phones`
        const result = await this.$axios.$get(url)
        commit(FETCH_PHONES, result)
    },
    async saveCustomerAddress({ commit }, address) {
        await this.$axios
            .$post(`${ENDPOINT}${ENDPOINT_ADDRESSES}`, address)
            .then(response => {
            commit(ADDRESS_SAVE, response);
        })
    },
    async deleteCustomerAddress({ commit }, id) {
        let url = `${ENDPOINT}${ENDPOINT_ADDRESSES}/${id}`
        await this.$axios
            .$delete(url)
            .then(response => {
            commit(ADDRESS_DELETE, response);
        })
    },
    async saveCustomerContact({ commit }, contact) {
        await this.$axios
            .$post(`${ENDPOINT}${ENDPOINT_CONTACTS}`, contact)
            .then(response => {
            commit(CONTACT_SAVE, response);
        })
    },
    async deleteCustomerContact({ commit }, id) {
        let url = `${ENDPOINT}${ENDPOINT_CONTACT}/${id}`
        await this.$axios
            .$delete(url)
            .then(response => {
            commit(CONTACT_DELETE, response);
        })
    },
    async saveCustomerPhone({ commit }, phone) {
        await this.$axios
            .$post(`${ENDPOINT}${ENDPOINT_PHONES}`, phone)
            .then(response => {
            commit(PHONE_SAVE, response);
        })
    },
    async deleteCustomerPhone({ commit }, id) {
        let url = `${ENDPOINT}${ENDPOINT_PHONES}/${id}`
        await this.$axios
            .$delete(url)
            .then(response => {
            commit(PHONE_DELETE, response);
        })
    },
}