const PAGE_SIZES = [5, 10, 15]
const PAGE_SIZE = 50
const PAGER_COUNT = 5
const PAGINATION_LAYOUT_DESKTOP = 'sizes, prev, pager, next, jumper, ->, total'
const PAGINATION_LAYOUT_MOBILE = 'prev, pager, next, ->'

const SET_PAGE_SIZE = 'SET_PAGE_SIZE'

export const state = () => ({
    pageSize: PAGE_SIZE,
    pageSizes: PAGE_SIZES,
    pagerCount: PAGER_COUNT,
    paginationLayoutDesktop: PAGINATION_LAYOUT_DESKTOP,
    paginationLayoutMobile: PAGINATION_LAYOUT_MOBILE
})

export const getters = {
    pageSize: state => state.pageSize,
    pageSizes: state => state.pageSizes,
    pagerCount: state => state.pagerCount,
    paginationLayoutDesktop: state => state.paginationLayoutDesktop,
    paginationLayoutMobile: state => state.paginationLayoutMobile
}

export const mutations = {
    [SET_PAGE_SIZE]: (state, size) => {
        state.pageSize = size
    }
}

export const actions = {
    setPageSize({ commit }, size) {
        commit(SET_PAGE_SIZE, size)
    }
}