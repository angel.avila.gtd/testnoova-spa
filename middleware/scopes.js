export default ({ $auth, route, redirect, error }) => {
    if (!!route.meta) {
        let permissions = route.meta.filter(e => (!!e.permissions)).map(e => e.permissions).map((e, i) => e[i])
        if (permissions.length > 0) {
            let can = false
            for (let i = 0; i < permissions.length; i++) {
                const p = permissions[i];
                can = can || $auth.hasScope(p)
            }
            if (!can) {
                error({ statusCode: 401 })
            }
        }
    }
}
