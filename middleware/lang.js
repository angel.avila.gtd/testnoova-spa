import getCookie from '@/utils/get-cookie'

export default function ({ store, route, redirect, req }) {
    const { lang } = getCookie(req)
    if (lang) {
        store.dispatch('setLang', lang)
    }

    let language = store.getters['lang']
    const routePath = route.path

    if ((routePath != `/login`) && (routePath != `/callback`) && language != 'es' && (routePath != `/${language}`) && (routePath.indexOf(`/${language}/`) === -1)) {
        return redirect({ path: `/${language}${routePath}`, query: route.query })
    }
}