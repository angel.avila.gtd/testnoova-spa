import SidebarItems from "@/config/app/sidebarItems"

export function getRender(permissions, auth) {
    if (typeof permissions == 'undefined') return true

    let can = false
    for (let i = 0; i < permissions.length; i++) {
        const p = permissions[i];
        can = can || auth.hasScope(p)
    }
    return can
}

export function fillMeta(route) {
    return fillMeta_(route, SidebarItems, '', 0)
}

function fillMeta_(route, items, helper, calls) {

    if (calls >= 1) {
        helper += '/';
    }

    for (let i = 0; i < items.length; i++) {
        const item = items[i];

        if (item.path === 'external-link') continue

        // console.log(`${helper + item.path} === ${route.path} : ${(helper + item.path) === route.path}`);
        // console.log(`hasChildren : ${!!item.children}`);

        if ((helper + item.path) === route.path) {
            route.meta = item.meta
        }

        if (!!item.children) {
            fillMeta_(route, item.children, helper + item.path, ++calls)
        }
    }
}

export function buildPredicates(predicates, filter) {
    if (filter.length == 0) {
        return '';
    }

    let result = ''

    predicates.forEach(p => {
        result += (p + "=" + filter + "&")
    });

    return result.substring(0, result.length - 1);
}

export function getLocalizedTitle(title, store) {
    let lang = store.getters['lang']
    if (title instanceof Object) {
        for (const l in title) {
            if (l === lang) {
                return title[l]
            }
        }
    }
    return title
}